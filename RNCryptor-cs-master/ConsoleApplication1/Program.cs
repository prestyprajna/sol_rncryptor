﻿using RNCryptor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the string to be Encrypted");
            string plaintext = Console.ReadLine();

            Console.WriteLine("Enter the password");
            string password = Console.ReadLine();

            Encryptor encryptorObj = new Encryptor();
            var encryptedData = encryptorObj.Encrypt(plaintext, password);
            Console.WriteLine("Encrypted Data=" + encryptedData);

            Decryptor decryptorObj = new Decryptor();
            var decryptedData = decryptorObj.Decrypt(encryptedData, password);
            Console.WriteLine("Decrypted Data=" + decryptedData);

        }
    }
}
